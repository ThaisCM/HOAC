﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pickCoinScript : MonoBehaviour
{

    //public AudioSource coinSource;
    public int coinValue;
    private SpriteRenderer _coinSpriteRenderer;
    private CapsuleCollider _coinCollider;

    public AudioSource audioCoin;

    private void Start()
    {
        coinValue = 1;
        audioCoin = gameObject.GetComponent<AudioSource>();
        _coinSpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        _coinSpriteRenderer.enabled = true;
    }

    
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
          
            audioCoin.Play();
            EventManager.TriggerEvent(new CoinsEvent(CoinsEvent.CoinsMethods.Add, coinValue));
            Debug.Log(" + " + coinValue + " coins!!!");
            gameObject.transform.localScale = new Vector3(0.0001f,0.0001f,0.0001f);
            StartCoroutine(DestroyCoin());
            
        }
    }

    private IEnumerator DestroyCoin()
    {
        
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
        
    }
}
