using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class PlayerBehind : Conditional
{
    private GameObject _Player;

    public override void OnAwake()
    {
        _Player = GameObject.FindGameObjectWithTag("Player");
    }
    public override TaskStatus OnUpdate()
	{
        if (_Player == null)
        {
            _Player = GameObject.FindGameObjectWithTag("Player");
        }
        Vector3 toTarget = (_Player.transform.position - transform.position).normalized;

        Vector3 forward = transform.TransformDirection(transform.forward);
        Vector3 other = _Player.transform.position - transform.position;

        if (Vector3.Dot(forward, other) < 0)
        {
            return TaskStatus.Success;
        }
        else
        {
            return TaskStatus.Failure;
        }
	}
}