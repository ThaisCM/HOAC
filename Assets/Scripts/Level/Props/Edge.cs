﻿using System;
using System.Collections;
using System.Collections.Generic;
using PixelCrushers.SceneStreamer;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Edge : MonoBehaviour
{
    public Zone Zone;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (Zone != null)
            {
                EventManager.TriggerEvent(new NewZoneEvent(Zone));
            }
        }
    }
}
