﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

[RequireComponent(typeof(Collider))]
public class Controller : MonoBehaviour
{
    [Header("Default Parameters")]
    public ControllerParameters DefaultParameters;
    
    public LayerMask GroundMask;

    [Header("Raycasts")]
    public int HorizontalDistance = 3;
    public float VerticalDistance = 0.1f;
    
    public bool DefaultGravityActive = true;

    private Collider m_Collider;
    private Agent m_Agent;
    private Rigidbody m_Rigidbody;

    private float m_CurrentGravity;
    private bool m_GravityActive;
    
    private Vector3 m_Velocity;
    private Vector3 m_MoveAmount;
    private RaycastHit m_HitInfo;

    public Vector3 Velocity { get { return m_Velocity; } }
    public ControllerParameters Parameters
    {
        get { return DefaultParameters; }
    }
    public ControllerState State { get; protected set; }
    public float FallSlowFactor { get; set; }

    protected virtual void Awake()
    {
        State = new ControllerState();

        m_Collider = GetComponent<Collider>();
        m_Agent = GetComponent<Agent>();
        m_Rigidbody = GetComponent<Rigidbody>();
    }
    
    void Update()
    {
        if (GameManager.Instance.Paused || Time.deltaTime == 0) return;
        
        //ApplyGravity();
        StartFrame();
        
        HorizontalCollisions();
        VerticalCollisions();
        
        transform.Translate(m_MoveAmount, Space.Self);

        //if (State.IsGrounded) m_Velocity.y = 0.0f;
    }

    private void StartFrame()
    {
        m_MoveAmount = m_Velocity * Time.deltaTime;
        
        State.Reset();
    }

    public bool GetGravityState()
    {
        return m_GravityActive;
    }
    
    public void SetGravityState(bool state)
    {
        m_GravityActive = state;
    }
    
    private void ApplyGravity()
    {
        m_CurrentGravity = Parameters.Gravity;
        
        if (m_Velocity.y > 0)
        {
            m_CurrentGravity = m_CurrentGravity / Parameters.AscentMultiplier;
        }
        if (m_Velocity.y < 0)
        {
            m_CurrentGravity = m_CurrentGravity * Parameters.FallMultiplier;
        }

        if (m_GravityActive)
        {
            m_Velocity.y += m_CurrentGravity * Time.deltaTime;

            if (FallSlowFactor != 0)
            {
                m_Velocity.y *= FallSlowFactor;
            }
        }
    }

    public void HorizontalCollisions()
    {
    }

    public void VerticalCollisions()
    {
        State.IsCollidingBelow = Physics.Raycast(transform.position, Vector3.down, out m_HitInfo, VerticalDistance, GroundMask);
        Debug.DrawLine(transform.position, transform.position - Vector3.up * VerticalDistance, Color.red);
    }
    
    public void SetAerialVelocity(float velocity)
    {
        m_Velocity.y = velocity;
    }
    
    public void SetVerticalVelocity(float velocity)
    {
        m_Velocity.z = velocity;
    }
    
    public void SetHorizontalVelocity(float velocity)
    {
        m_Velocity.x = velocity;
    }
}
