﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class PlayerInput : MonoBehaviour, EventListener<CharacterEvent>, EventListener<BlockCharacter>, EventListener<TransitionEvent>
{
    private Character m_Character;

    private const float DEADZONE_VALUE = 0.4f;

    public Player Player { get; set; }
    
    public InputActionComponent.Action Horizontal { get; set; }
    public InputActionComponent.Action Vertical { get; set; }
    public InputActionComponent.Action Jump { get; set; }
    public InputActionComponent.Action Interact { get; set; }
    public InputActionComponent.Action Attack { get; set; }
    public InputActionComponent.Action Pause { get; set; }
    public InputActionComponent.Action Map { get; set; }
    public InputActionComponent.Action Back { get; set; }
    public InputActionComponent.Action Inventory { get; set; }
    public InputActionComponent.Action Skip { get; set; }
    public InputActionComponent.Action Item1 { get; set; }
    public InputActionComponent.Action Item2 { get; set; }
    public InputActionComponent.Action Item3 { get; set; }
    public InputActionComponent.Action Item4 { get; set; }
    public InputActionComponent.Action Run { get; set; }
    public InputActionComponent.Action Dodge { get; set; }
    public InputActionComponent.Action Block { get; set; }
    public InputActionComponent.Action Push { get; set; }
    public InputActionComponent.Action SlowAttack { get; set; }
    public InputActionComponent.Action FastAttack { get; set; }
    public InputActionComponent.Action Stealth { get; set; }
    public InputActionComponent.Action LookAtHorizontal { get; set; }
    public InputActionComponent.Action LookAtVertical { get; set; }
    public InputActionComponent.Action Diary { get; set; }

    private void Awake()
    {
        Player = ReInput.players.GetPlayer(0);

        m_Character = GetComponent<Character>();
    }

    void OnEnable()
    {
        this.EventStartListening<CharacterEvent>();
        this.EventStartListening<BlockCharacter>();
        this.EventStartListening<TransitionEvent>();
    }

    void OnDisable()
    {
        this.EventStopListening<CharacterEvent>();
        this.EventStopListening<BlockCharacter>();
        this.EventStopListening<TransitionEvent>();
    }

    public void SetController(int controllerID)
    {
        Player = ReInput.players.GetPlayer(controllerID);
        
        Horizontal = new InputActionComponent.Action(InputActionComponent.Actions.Horizontal, Player);
        Vertical = new InputActionComponent.Action(InputActionComponent.Actions.Vertical, Player);
        Jump = new InputActionComponent.Action(InputActionComponent.Actions.Jump, Player);
        Interact = new InputActionComponent.Action(InputActionComponent.Actions.Interact, Player);
        Attack = new InputActionComponent.Action(InputActionComponent.Actions.Attack, Player);
        Pause = new InputActionComponent.Action(InputActionComponent.Actions.Pause, Player);
        Map = new InputActionComponent.Action(InputActionComponent.Actions.Map, Player);
        Back = new InputActionComponent.Action(InputActionComponent.Actions.Back, Player);
        Inventory = new InputActionComponent.Action(InputActionComponent.Actions.Inventory, Player);
        Skip = new InputActionComponent.Action(InputActionComponent.Actions.Skip, Player);
        Item1 = new InputActionComponent.Action(InputActionComponent.Actions.Item1, Player);
        Item2 = new InputActionComponent.Action(InputActionComponent.Actions.Item2, Player);
        Item3 = new InputActionComponent.Action(InputActionComponent.Actions.Item3, Player);
        Item4 = new InputActionComponent.Action(InputActionComponent.Actions.Item4, Player);
        Run = new InputActionComponent.Action(InputActionComponent.Actions.Run, Player);
        Dodge = new InputActionComponent.Action(InputActionComponent.Actions.Dodge, Player);
        Block = new InputActionComponent.Action(InputActionComponent.Actions.Block, Player);
        Push = new InputActionComponent.Action(InputActionComponent.Actions.Push, Player);
        SlowAttack = new InputActionComponent.Action(InputActionComponent.Actions.SlowAttack, Player);
        FastAttack = new InputActionComponent.Action(InputActionComponent.Actions.FastAttack, Player);
        Stealth = new InputActionComponent.Action(InputActionComponent.Actions.Stealth, Player);
        LookAtHorizontal = new InputActionComponent.Action(InputActionComponent.Actions.LookAtHorizontal, Player);
        LookAtVertical = new InputActionComponent.Action(InputActionComponent.Actions.LookAtVertical, Player);
        Diary = new InputActionComponent.Action(InputActionComponent.Actions.Diary, Player);
    }

    public float GetDeadzone()
    {
        float deadzone;
        if (Player.controllers.joystickCount > 0)
            deadzone = Player.controllers.Joysticks[0].GetCalibrationMapSaveData().map.Axes[0].deadZone +
                       DEADZONE_VALUE;
        else
        {
            deadzone = 0;
        }

        return deadzone;
    }
    
    public bool VerticalAxisNeutral()
    {
        if (Vertical.GetAxis() > GetDeadzone() || Vertical.GetAxis() < -GetDeadzone())
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    public bool HorizontalAxisNeutral()
    {
        if (Horizontal.GetAxis() > GetDeadzone() || Horizontal.GetAxis() < -GetDeadzone())
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    public bool RightVerticalAxisNeutral()
    {
        if (LookAtVertical.GetAxis() > GetDeadzone() || LookAtVertical.GetAxis() < -GetDeadzone())
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    public bool RightHorizontalAxisNeutral()
    {
        if (LookAtHorizontal.GetAxis() > GetDeadzone() || LookAtHorizontal.GetAxis() < -GetDeadzone())
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void DisableAllActions()
    {
        Horizontal.IsEnabled = false;
        Vertical.IsEnabled = false;
        Jump.IsEnabled = false;
        Interact.IsEnabled = false;
        Attack.IsEnabled = false;
        Pause.IsEnabled = false;
        Map.IsEnabled = false;
        Back.IsEnabled = false;
        Inventory.IsEnabled = false;
        Skip.IsEnabled = false;
        Item1.IsEnabled = false;
        Item2.IsEnabled = false;
        Item3.IsEnabled = false;
        Item4.IsEnabled = false;
        Run.IsEnabled = false;
        Dodge.IsEnabled = false;
        Block.IsEnabled = false;
        Push.IsEnabled = false;
        SlowAttack.IsEnabled = false;
        FastAttack.IsEnabled = false;
        Stealth.IsEnabled = false;
        LookAtHorizontal.IsEnabled = false;
        LookAtVertical.IsEnabled = false;
        Diary.IsEnabled = false;
    }
    
    public void EnableAllActions()
    {
        Horizontal.IsEnabled = true;
        Vertical.IsEnabled = true;
        Jump.IsEnabled = true;
        Interact.IsEnabled = true;
        Attack.IsEnabled = true;
        Pause.IsEnabled = true;
        Map.IsEnabled = true;
        Back.IsEnabled = true;
        Inventory.IsEnabled = true;
        Skip.IsEnabled = true;
        Item1.IsEnabled = true;
        Item2.IsEnabled = true;
        Item3.IsEnabled = true;
        Item4.IsEnabled = true;
        Run.IsEnabled = true;
        Dodge.IsEnabled = true;
        Block.IsEnabled = true;
        Push.IsEnabled = true;
        SlowAttack.IsEnabled = true;
        FastAttack.IsEnabled = true;
        Stealth.IsEnabled = true;
        LookAtHorizontal.IsEnabled = true;
        LookAtVertical.IsEnabled = true;
        Diary.IsEnabled = true;
    }
    
    public void DisableMovementActions()
    {
        Horizontal.IsEnabled = false;
        Vertical.IsEnabled = false;
        Jump.IsEnabled = false;
        Interact.IsEnabled = false;
        Attack.IsEnabled = false;
        Item1.IsEnabled = false;
        Item2.IsEnabled = false;
        Item3.IsEnabled = false;
        Item4.IsEnabled = false;
        Run.IsEnabled = false;
        Dodge.IsEnabled = false;
        Block.IsEnabled = false;
        Push.IsEnabled = false;
        SlowAttack.IsEnabled = false;
        FastAttack.IsEnabled = false;
        Stealth.IsEnabled = false;
        LookAtHorizontal.IsEnabled = false;
        LookAtVertical.IsEnabled = false;
    }

    private IEnumerator DelayedEnableAllActions()
    {
        yield return new WaitForEndOfFrame();
        EnableAllActions();
    }
    
    public void OnEvent(CharacterEvent eventType)
    {
        if (eventType.Character == m_Character)
        {
            switch (eventType.CharacterEventType)
            {
                case CharacterEvent.Type.RespawnStart:
                    DisableAllActions();
                    break;
                case CharacterEvent.Type.RespawnEnd:
                    StartCoroutine(DelayedEnableAllActions());
                    break;
            }
        }
    }
    
    public void OnEvent(BlockCharacter eventType)
    {
        switch (eventType.Block)
        {
            case BlockCharacter.Type.Block:
                DisableAllActions();
                break;
            case BlockCharacter.Type.Unblock:
                StartCoroutine(DelayedEnableAllActions());
                break;
            case BlockCharacter.Type.BlockMovement:
                DisableMovementActions();
                break;
        }
    }
    
    public void OnEvent(TransitionEvent eventType)
    {
        switch (eventType.TransitionType)
        {
            case TransitionEvent.Type.Start:
                DisableAllActions();
                break;
            case TransitionEvent.Type.End:
                StartCoroutine(DelayedEnableAllActions());
                break;
        }
    }
}
