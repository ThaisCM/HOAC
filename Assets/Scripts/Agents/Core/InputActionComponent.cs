﻿using System.Collections.Generic;
using Rewired;
using UnityEngine;

public class InputActionComponent : MonoBehaviour
{
    public enum Actions
    {
        Horizontal,
        Vertical,
        Jump,
        Interact,
        Attack,
        Pause,
        Map,
        Back,
        Inventory,
        Skip,
        Item1,
        Item2,
        Item3, 
        Item4,
        Run,
        Dodge, 
        Block, 
        Push,
        SlowAttack,
        FastAttack,
        Stealth,
        LookAtHorizontal,
        LookAtVertical,
        Diary
    }

    public class Action
    {
        public Actions ActionButton;
        private Player m_Player;
        public bool IsEnabled = true;

        public Action(Actions action, Player player)
        {
            ActionButton = action;
            m_Player = player;
        }

        protected Dictionary<int, string> ButtonsToName = new Dictionary<int, string>()
        {
            {(int) Actions.Horizontal, "Horizontal"},
            {(int) Actions.Vertical, "Vertical"},
            {(int) Actions.Jump, "Jump"},
            {(int) Actions.Interact, "Interact"},
            {(int) Actions.Attack, "Attack"},
            {(int) Actions.Pause, "Pause"},
            {(int) Actions.Map, "Map"},
            {(int) Actions.Back, "Back"},
            {(int) Actions.Inventory, "Inventory"},
            {(int) Actions.Skip, "Skip"},
            {(int) Actions.Item1, "Item1"},
            {(int) Actions.Item2, "Item2"},
            {(int) Actions.Item3, "Item3"},
            {(int) Actions.Item4, "Item4"},
            {(int) Actions.Run, "Run"},
            {(int) Actions.Dodge, "Dodge"},
            {(int) Actions.Block, "Block"},
            {(int) Actions.Push, "Push"},
            {(int) Actions.SlowAttack, "SlowAttack"},
            {(int) Actions.FastAttack, "FastAttack"},
            {(int) Actions.Stealth, "Stealth"},
            {(int) Actions.LookAtHorizontal, "LookAtHorizontal"},
            {(int) Actions.LookAtVertical, "LookAtVertical"},
            {(int) Actions.Diary, "Diary"}
        };

        public bool GetButton()
        {
            if (IsEnabled)
            {
                if (m_Player.GetButton(ButtonsToName[(int) ActionButton]))
                {
                    return true;
                }
            }
            return false;
        }

        public bool GetButtonDown()
        {
            if (IsEnabled)
            {
                if (m_Player.GetButtonDown(ButtonsToName[(int) ActionButton]))
                {
                    return true;
                }
            }
            return false;
        }
        
        public bool GetButtonUp()
        {
            if (IsEnabled)
            {
                if (m_Player.GetButtonUp(ButtonsToName[(int)ActionButton]))
                {
                    return true;
                }
            }
            return false;
        }

        public bool GetButtonLongPressDown()
        {
            if (IsEnabled)
            {
                if (m_Player.GetButtonLongPressDown(ButtonsToName[(int)ActionButton]))
                {
                    return true;
                }
            }
            return false;
        }
        
        public bool GetButtonLongPressUp()
        {
            if (IsEnabled)
            {
                if (m_Player.GetButtonLongPressUp(ButtonsToName[(int)ActionButton]))
                {
                    return true;
                }
            }
            return false;
        }

        public bool GetButtonShortPressDown()
        {
            if (IsEnabled)
            {
                if (m_Player.GetButtonShortPressDown(ButtonsToName[(int)ActionButton]))
                {
                    return true;
                }
            }
            return false;
        }

        public bool GetButtonShortPressUp()
        {
            if (IsEnabled)
            {
                if (m_Player.GetButtonShortPressUp(ButtonsToName[(int)ActionButton]))
                {
                    return true;
                }
            }
            return false;
        }
        
        public float GetAxis()
        {
            if (IsEnabled)
            {
                return m_Player.GetAxis(ButtonsToName[(int)ActionButton]);
            }
            else
            {
                return 0;
            }
        }
    }
}
