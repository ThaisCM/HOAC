﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct LevelingEvents
{
    public enum EventType { LevelUp, Reset }

    public EventType Type;

    public LevelingEvents(EventType type)
    {
        Type = type;
    }
}

[Serializable]
public class LevelSystem
{
    public const int XP_ADJUSTMENT = 5;
    public const int XP_BASE = 500;
    public const int XP_INCREMENT = 2;
    public const int MAX_LEVEL = 50;

    public int Level { get; set; }
    public int XP { get; set; }
    public int NextLevelXP { get; set; }

    public void Initialize()
    {
        Level = 1;
        XP = 0;
        NextLevelXP = RequiredXpNextLevel();
    }

    public void Reset()
    {
        Initialize();

        EventManager.TriggerEvent(new LevelingEvents(LevelingEvents.EventType.Reset));
    }

    public int RequiredXpNextLevel()
    {
        return Mathf.RoundToInt(XP_BASE * (Mathf.Pow(Level, XP_INCREMENT) / XP_ADJUSTMENT));
    }

    public void AddXp(int xp)
    {
        if (Level >= MAX_LEVEL)
        {
            return;
        }

        if (xp == 0)
        {
            return;
        }

        XP += xp;
        while (XP >= NextLevelXP)
        {
            if (Level >= MAX_LEVEL)
            {
                XP = 0;
                return;
            }

            XP -= NextLevelXP;
            LevelUp();
        }
    }

    private void LevelUp()
    {
        Level++;
        NextLevelXP = RequiredXpNextLevel();
        
        EventManager.TriggerEvent(new LevelingEvents(LevelingEvents.EventType.LevelUp));
    }
}
