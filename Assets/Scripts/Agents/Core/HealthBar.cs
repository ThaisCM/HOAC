﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public SliderBar Hp;
    public float ShowTime = 3f;

    protected Health m_Health;

    protected bool m_Show;
    protected float m_Time = 0f;
    private CanvasGroup m_CanvasGroup;

    void Awake()
    {
        m_Health = GetComponentInParent<Health>();
        m_CanvasGroup = GetComponent<CanvasGroup>();
    }

    void Update()
    {
        if (!m_Show)
        {
            return;
        }

        transform.position = new Vector3(transform.parent.position.x, transform.parent.position.y + 0.2f, transform.parent.position.z);
        transform.rotation = Quaternion.identity;

        m_Time -= Time.deltaTime;
        if (m_Time <= 0)
        {
            HideBar();
        }
    }

    public void UpdateHealthBar()
    {
        Hp.UpdateBar(m_Health.CurrentHealth, 0f, m_Health.MaximumHealth);
    }

    public void ShowBar()
    {
        if (!m_Show)
        {
            m_CanvasGroup.alpha = 1;
            m_Show = true;
        }

        UpdateHealthBar();
        m_Time = ShowTime;
    }

    public void HideBar()
    {
        m_CanvasGroup.alpha = 0;
        m_Show = false;
    }
}
