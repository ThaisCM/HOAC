﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Agent))]
[RequireComponent(typeof(Controller))]
public abstract class AgentMovement : MonoBehaviour
{
    private float WalkingSpeed = 1f;
    private float RunningSpeed = 3f;

    protected Controller m_Controller;
    protected Agent m_Agent;
    protected Animator m_Animator;

    public float MovementSpeed { get; set; }

    public abstract void Stop();

    public abstract void MoveTo(Vector3 position);

    protected abstract void EvaluateState();
    
    protected abstract void EvaluateMovement();
    
    protected abstract bool EvaluateConditions();

    protected abstract void CalculateMovementValue();

    public abstract void NeutralMovement();
    
    protected virtual void Awake()
    {
        m_Controller = GetComponent<Controller>();
        m_Agent = GetComponent<Agent>();
        m_Animator = GetComponent<Animator>();
    }

    protected virtual void Start()
    {
        WalkingSpeed = m_Agent.CurrentStats.WalkingSpeed;
        RunningSpeed = m_Agent.CurrentStats.RunningSpeed;
        MovementSpeed = WalkingSpeed;
    }

    
    protected virtual void Update()
    {
        if (GameManager.Instance.Paused || Time.deltaTime == 0)
        {
            return;
        }
        
        if (!EvaluateConditions())
        {
            return;
        }

        WalkingSpeed = m_Agent.CurrentStats.WalkingSpeed;
        RunningSpeed = m_Agent.CurrentStats.RunningSpeed;
        
        switch (m_Agent.MovementState.CurrentState)
        {
            case AgentStates.MovementStates.Idle:
                MovementSpeed = 0f;
                break;
            case AgentStates.MovementStates.Stealth:
                MovementSpeed = WalkingSpeed;
                break;
            case AgentStates.MovementStates.Walking:
                MovementSpeed = WalkingSpeed;
                break;
            case AgentStates.MovementStates.Running:
                MovementSpeed = RunningSpeed;
                break;
        }
   
        EvaluateState();
        EvaluateMovement();
        
        CalculateMovementValue();
        UpdateAnimatorValues();
    }

    public virtual void DisableMovement()
    {
        Stop();

        UpdateAnimatorValues();

        m_Agent.FreedomState.ChangeState(AgentStates.AgentMovementConditions.Immobilized);
    }

    public virtual void EnableMovement()
    {
        m_Agent.FreedomState.ChangeState(AgentStates.AgentMovementConditions.Free);
    }

    protected virtual void UpdateAnimatorValues()
    {
        m_Animator.SetFloat("Speed", MovementSpeed);
    }
}
