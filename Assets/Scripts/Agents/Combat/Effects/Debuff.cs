﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Debuff : Effect
{
    public Debuff(float duration, bool stackable) : base(duration, stackable)
    {

    }
}

public abstract class StatDebuff : Debuff
{
    public StatDebuff(float duration, bool stackable) : base(duration, stackable)
    {

    }
}
