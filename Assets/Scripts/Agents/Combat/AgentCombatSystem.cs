﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Controller))]
[RequireComponent(typeof(Agent))]
public class AgentCombatSystem : MonoBehaviour
{
    protected Animator m_Animator;
    protected Controller m_Controller;
    protected Agent m_Agent;
    protected AgentEffects m_Effects;

    public CombatTechnique CurrentTechnique { get; set; }
    
    public HitboxBehaviour MeleeHitboxBehaviour { get; protected set; }
    public HurtboxBehaviour HurtboxBehaviour { get; protected set; }
    public DefenseboxBehaviour DefenseboxBehaviour { get; protected set; }
    
    public bool IsPerformingTechnique
    {
        get
        {
            return m_Agent.CombatState.CurrentState == AgentStates.CombatStates.Attacking ||
                   m_Agent.CombatState.CurrentState == AgentStates.CombatStates.Blocking ||
                   m_Agent.CombatState.CurrentState == AgentStates.CombatStates.Dodging;
        }
    }

    protected virtual void Awake()
    {
        m_Animator = GetComponent<Animator>();
        m_Controller = GetComponent<Controller>();
        m_Agent = GetComponent<Agent>();
        m_Effects = GetComponent<AgentEffects>();

        MeleeHitboxBehaviour = GetComponentInChildren<HitboxBehaviour>();
        HurtboxBehaviour = GetComponentInChildren<HurtboxBehaviour>();
        DefenseboxBehaviour = GetComponentInChildren<DefenseboxBehaviour>();
    }

    public virtual void OnTechniqueEnd()
    {
        if (CurrentTechnique != null) CurrentTechnique.TechniqueEnd(m_Agent);

        CurrentTechnique = null;
        
        m_Agent.CombatState.ChangeState(AgentStates.CombatStates.NoCombat);
    }

    public virtual void OnTechniqueStart(CombatTechnique technique)
    {
        CurrentTechnique = technique;

        switch (technique.TechniqueType)
        {
            case CombatTechnique.Type.Attack:
                m_Agent.CombatState.ChangeState(AgentStates.CombatStates.Attacking);
                break;
            case CombatTechnique.Type.Block:
                m_Agent.CombatState.ChangeState(AgentStates.CombatStates.Blocking);
                break;
            case CombatTechnique.Type.Dodge:
                m_Agent.CombatState.ChangeState(AgentStates.CombatStates.Dodging);
                break;
        }

        if (m_Agent.MovementState.CurrentState == AgentStates.MovementStates.Stealth)
        {
            m_Agent.ExitStealth();
        }

        CurrentTechnique.TechniqueStart(m_Agent);
    }

    public virtual void PerformTechnique()
    {
        CurrentTechnique.PerformTechnique(m_Agent);
    }

    public virtual void FinishTechnique()
    {
        if (CurrentTechnique != null) CurrentTechnique.FinishTechnique(m_Agent);
    }

    public virtual void TriggerTechnique(string parameter, bool isBool = false)
    {
        if(isBool) m_Animator.SetBool(parameter, true);
        else m_Animator.SetTrigger(parameter);
    }

    protected bool CheckAvailability()
    {
        if (GameManager.Instance.Paused || Time.deltaTime == 0f)
        {
            return false;
        }

        if (m_Agent.ConditionState.CurrentState != AgentStates.AgentConditions.Normal)
        {
            return false;
        }

        return true;
    }

    public virtual DamageValues CalculateDamageDone(GameObject target, DamageTechnique technique)
    {
        int baseDamage = technique.Damage.Value;

        bool isCritical = false;
        if (Random.value <= m_Agent.CurrentStats.CriticalChance)
        {
            baseDamage = (int)(baseDamage * m_Agent.CurrentStats.CriticalDamage);
            isCritical = true;
        }

        return new DamageValues(baseDamage, isCritical);
    }
}
