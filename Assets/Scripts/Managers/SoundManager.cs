﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : PersistentSingleton<SoundManager>
{
   [Header("Music")]
   public AudioClip MenuAudioClip;

   [Range(0,1)]
   public float MusicVolume = 0.5f;
   
   [Header("Mixer Fade")]
   public float FadeDuration = 10f;

   [Header("Sound Effects")]
   [Range(0, 1)]
   public float SfxVolume = 1f;
   public AudioMixer AudioMixer;
   public AudioMixerGroup InGameAudioMixer;
   
   private AudioSource m_BackgroundMusic;
   private List<AudioSource> m_LoopingSounds;

   protected override void Awake()
   {
      base.Awake();

      m_BackgroundMusic = GetComponent<AudioSource>();
   }
   
   void Start()
   {
      m_LoopingSounds = new List<AudioSource>();
   }
   
   public IEnumerator _PlayBackgroundMusic(AudioClip clip)
   {
      if (m_BackgroundMusic.clip == clip)
      {
         yield break;
      }

      StartCoroutine(StartFade(m_BackgroundMusic.outputAudioMixerGroup.audioMixer, "BackgroundMusic_Volume", FadeDuration, 0f));

      yield return new WaitForSeconds(FadeDuration);

      m_BackgroundMusic.clip = clip;
      m_BackgroundMusic.volume = MusicVolume;
      m_BackgroundMusic.loop = true;
      m_BackgroundMusic.Play();


      StartCoroutine(StartFade(m_BackgroundMusic.outputAudioMixerGroup.audioMixer, "BackgroundMusic_Volume", FadeDuration, MusicVolume));
   }
   
   public void PlayBackgroundMusic(AudioClip clip)
   {
      StartCoroutine(_PlayBackgroundMusic(clip));
   }
   
   public void SetMusicVolume(float volume)
   {
      MusicVolume = volume;
      if(m_BackgroundMusic != null) m_BackgroundMusic.volume = MusicVolume;
   }
   
   public void SetSFXVolume(float volume)
   {
      SfxVolume = volume;
      AudioMixer.SetFloat("SFX_Volume", VolumeToDecibels(SfxVolume));
   }
   
   public void StopBackgroundMusic()
   {
      m_BackgroundMusic.Stop();
   }
   
   public void ResumeBackgroundMusic()
   {
      m_BackgroundMusic.Play();
   }
   
   public AudioSource PlaySound(AudioClip sfx, Vector3 location, bool loop = false)
   {
      GameObject temporaryAudioHost = new GameObject("TempAudio");
      
      temporaryAudioHost.transform.position = location;
      
      AudioSource audioSource = temporaryAudioHost.AddComponent<AudioSource>() as AudioSource;
      
      audioSource.outputAudioMixerGroup = InGameAudioMixer;
      
      audioSource.clip = sfx;

      audioSource.volume = SfxVolume;
      
      audioSource.loop = loop;
      
      audioSource.rolloffMode = AudioRolloffMode.Linear;
      
      audioSource.Play();

      if (!loop)
      {
         Destroy(temporaryAudioHost, sfx.length);
      }
      else
      {
         m_LoopingSounds.Add(audioSource);
      }
      return audioSource;
   }
   
   public void ApplySoundValues(float musicValue, float sfxValue)
   {
      SetMusicVolume(musicValue);
      SetSFXVolume(sfxValue);
   }
   
   public float VolumeToDecibels(float volume)
   {
      if (volume > 0) return Mathf.Log10(volume) * 20;
      else return -80f;
   }
   
   public IEnumerator StartFade(AudioMixer audioMixer, string exposedParam, float duration, float targetVolume)
   {
      float currentTime = 0;
      float currentVol;
      audioMixer.GetFloat(exposedParam, out currentVol);
      currentVol = Mathf.Pow(10, currentVol / 20);

      float targetValue = Mathf.Clamp(targetVolume, 0.0001f, 1);

      while (currentTime < duration)
      {
                
         currentTime += Time.deltaTime;
         float newVol = Mathf.Lerp(currentVol, targetValue, currentTime / duration);
         audioMixer.SetFloat(exposedParam, Mathf.Log10(newVol) * 20);
         yield return null;
      }

      yield break;
   }
}
