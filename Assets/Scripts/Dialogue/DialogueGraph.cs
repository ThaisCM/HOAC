﻿using System.Linq;
using UnityEngine;
using XNode;

[CreateAssetMenu(fileName = "DialogueGraph", menuName = "G1/Dialogue Engine/Dialogue Graph")]
public class DialogueGraph : NodeGraph
{
    public DialogueSpeaker Agent;
    public DialogueSpeaker Conversator;
    
    public Transform Trigger { get; set; }
    
    public Transform Opener { get; set; }
    
    public int TimesTalked { get; set; }

    public void Initialize(Transform trigger, Transform opener, int timesTalked)
    {
        Trigger = trigger;
        Opener = opener;
        TimesTalked = timesTalked;
    }

    public DialogueBaseNode GetRootNode()
    {
        DialogueStart dialogueStart = nodes.Find(x => x is DialogueStart) as DialogueStart;
        if (dialogueStart != null) return dialogueStart;
        else
        {
            Chat chat = nodes.Find(x => x is Chat && x.Inputs.All(y => !y.IsConnected)) as Chat;
            if (chat != null) return chat;
            else
            {
                DialogueDecision node = nodes.Find(x => x is DialogueDecision && x.Inputs.All(y => !y.IsConnected)) as DialogueDecision;
                return node;
            }
        }
    }
}
