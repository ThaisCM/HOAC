﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

[CustomNodeEditor(typeof(OpenShop))]
public class OpenShopEditor : NodeEditor
{
    private SerializedProperty m_Shop;

    public override void OnBodyGUI()
    {
        serializedObject.Update();

        EventDialogue node = target as EventDialogue;

        NodeEditorGUILayout.PortField(target.GetInputPort("input"), GUILayout.Width(100));

        EditorGUILayout.Space();

        serializedObject.ApplyModifiedProperties();
    }

    public override int GetWidth()
    {
        return 336;
    }
}
