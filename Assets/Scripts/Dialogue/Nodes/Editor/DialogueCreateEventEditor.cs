﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

[CustomNodeEditor(typeof(DialogueCreateEvent))]
public class DialogueCreateEventEditor : NodeEditor
{
    public override void OnBodyGUI()
    {
        serializedObject.Update();

        DialogueCreateEvent node = target as DialogueCreateEvent;

        GUILayout.BeginHorizontal();
        NodeEditorGUILayout.PortField(GUIContent.none, target.GetInputPort("input"), GUILayout.MinWidth(0));
        NodeEditorGUILayout.PortField(GUIContent.none, target.GetOutputPort("output"), GUILayout.MinWidth(0));
        GUILayout.EndHorizontal();
        GUILayout.Space(-10);

        EditorGUILayout.PropertyField(serializedObject.FindProperty("Event"), new GUIContent("Event"));

        serializedObject.ApplyModifiedProperties();
    }

    public override int GetWidth()
    {
        return 300;
    }
}
