﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[NodeTint("FF00E1")]
public class DialogueStart : DialogueBaseNode
{
    public override void Trigger()
    {
        DialogueManager.Instance.ExecuteNextNode(0);
    }
}
