﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

public abstract class DialogueBaseNode : Node
{
    [Input(backingValue = ShowBackingValue.Never, typeConstraint = TypeConstraint.Inherited)] public DialogueBaseNode input;
    [Output(backingValue = ShowBackingValue.Never, instancePortList = true)] public DialogueBaseNode output;
    
    public virtual void Trigger()
    {
        
    }

    public virtual void Skip()
    {
        
    }

    public virtual DialogueBaseNode GetNextNode(int index)
    {
        NodePort port = GetOutputPort("output");
        
        if(port == null) throw new SystemException("No NodePort");
        NodePort nextPort = port.GetConnection(index);
        
        return nextPort.node as DialogueBaseNode;
    }

    public override object GetValue(NodePort port)
    {
        return null;
    }
}
