﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[NodeTint("#FF0000")]
public class DialogueCreateEvent : DialogueBaseNode
{
    public string Event;

    public override void Trigger()
    {
        EventManager.TriggerEvent(new GameEvent(Event));
        DialogueManager.Instance.ExecuteNextNode(0);
    }

    public override void Skip()
    {
        EventManager.TriggerEvent(new GameEvent(Event));
    }
}
