﻿using System.Collections;
using System.Collections.Generic;
using PetoonsStudio.Tools;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TranslatedText))]
public class TranslatedTextEditor : Editor
{
    /// <summary>
    /// OnInspectorGUI
    /// </summary>
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        TranslatedText text = (TranslatedText)target;

        EditorGUILayout.LabelField("Translation", StaticLocalizationManager.Translation(text.PhraseType, text.PhraseKey));
    }
}
