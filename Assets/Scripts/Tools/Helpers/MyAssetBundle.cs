﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class MyAssetBundle
{
    public static AssetBundle LoadAssetBundleFromFile(string bundleURL)
    {
        AssetBundle bundle = null;
        try
        {
            string path;
#if UNITY_STANDALONE_WIN
            path = System.IO.Path.Combine(Application.streamingAssetsPath, "AssetBundles/Windows/" + bundleURL);
#elif UNITY_STANDALONE_OSX
            path = System.IO.Path.Combine(Application.streamingAssetsPath, "AssetBundles/OSX/" + bundleURL);
#elif UNITY_STANDALONE_LINUX
            path = System.IO.Path.Combine(Application.streamingAssetsPath, "AssetBundles/Linux/" + bundleURL);
#endif
            if (File.Exists(path))
            {
                bundle = AssetBundle.LoadFromFile(path);
            }
        }
        catch (System.Exception e)
        {
            throw e;
        }

        if (bundle == null)
        {
            throw new System.Exception("Bundle miss");
        }
        else
        {
            return bundle;
        }
    }
}
