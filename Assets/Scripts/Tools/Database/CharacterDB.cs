﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "CharacterDB", menuName = "G1/Database/CharacterDB")]
public class CharacterDB : AbstractDatabase<CharacterSO>
{
    private static CharacterDB m_Instance = null;

    public static CharacterDB Instance
    {
        get
        {
            if (m_Instance == null)
            {
                LoadDatabase();
            }

            return m_Instance;
        }
    }

    protected override void OnAddObject(CharacterSO t)
    {
#if UNITY_EDITOR
        t.name = "CH" + t.Id;
        AssetDatabase.AddObjectToAsset(t, this);
        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
#endif
    }

    protected override void OnRemoveObject(CharacterSO t)
    {
#if UNITY_EDITOR
        AssetDatabase.RemoveObjectFromAsset(t);
        EditorUtility.SetDirty(this);
#endif
    }

    public static void LoadDatabase()
    {
#if UNITY_EDITOR
        m_Instance = (CharacterDB)AssetDatabase.LoadAssetAtPath("Assets/ScriptableObjects/Model/CharacterDB.asset", typeof(CharacterDB));
#else
            AssetBundle bundle = MyAssetBundle.LoadAssetBundleFromFile("g1/agents/characters");
            m_Instance = bundle.LoadAsset<CharacterDB>("CharacterDB");
            bundle.Unload(false);
#endif
    }

    public static void ClearDatabase()
    {
        m_Instance = null;
    }
}
