﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyDB", menuName = "G1/Database/EnemiesDB")]
public class EnemyDB : AbstractDatabase<EnemySO>
{
    private static EnemyDB m_Instance = null;

    public static EnemyDB Instance
    {
        get
        {
            if (m_Instance == null)
            {
                LoadDatabase();
            }

            return m_Instance;
        }
    }

    protected override void OnAddObject(EnemySO t)
    {
#if UNITY_EDITOR

#endif
    }

    protected override void OnRemoveObject(EnemySO t)
    {
#if UNITY_EDITOR
        AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(t));
        AssetDatabase.SaveAssets();
        EditorUtility.SetDirty(this);
#endif
    }

    public static void LoadDatabase()
    {
#if UNITY_EDITOR
        m_Instance = (EnemyDB)AssetDatabase.LoadAssetAtPath("Assets/ScriptableObjects/Model/EnemyDB.asset", typeof(EnemyDB));
#else
            AssetBundle bundle = MyAssetBundle.LoadAssetBundleFromFile("g1/agents/enemies");
            m_Instance = bundle.LoadAsset<EnemyDB>("EnemyDB");
            bundle.Unload(false);
#endif
    }

    public static void ClearDatabase()
    {
        m_Instance = null;
    }
}
