﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Shop", menuName = "G1/Shop Engine/Shop")]
public class Shop : ScriptableObject, ISerialize<SerializedShop>
{
    public enum Category
    {
        All,
        Consumable,
        Usable
    }

    public string ShopName;
    public ShopItemSet[] ItemSets;

    public void Deserialize(SerializedShop data)
    {
        foreach (var set in ItemSets)
        {
            set.Unlocked = data.ItemSets[set.ID].Unlocked;
            foreach (var item in set.Set)
            {
                item.Quantity = data.ItemSets[set.ID].Items[item.Item.ID];
            }
        }
    }

    public SerializedShop Serialize()
    {
        SerializedShop shop = new SerializedShop();

        foreach (var set in ItemSets)
        {
            shop.ItemSets = new Dictionary<int, SerializedShopItemSet>();

            SerializedShopItemSet itemSet = new SerializedShopItemSet
            {
                Unlocked = set.Unlocked,
                Items = new Dictionary<int, int>()
            };

            foreach (var item in set.Set)
            {
                itemSet.Items.Add(item.Item.ID, item.Quantity);
            }
            
            shop.ItemSets.Add(set.ID, itemSet);
        }

        return shop;
    }
    
    public List<ShopItem> GetShopItemsCategory(Category category)
    {
        List<ShopItem> filteredList = new List<ShopItem>();

        foreach (var itemSet in ItemSets)
        {
            if (itemSet.Unlocked)
            {
                foreach (var item in itemSet.Set)
                {
                    if (item.Quantity <= 0)
                    {
                        continue;
                    }
                       
                    switch (category)
                    {
                        case Category.All:
                            filteredList.Add(item);
                            break;
                        case Category.Consumable:
                            if (item.Item is ConsumableItem) filteredList.Add(item);
                            break;
                        case Category.Usable:
                            if (item.Item is UsableItem) filteredList.Add(item);
                            break;
                    }
                }
            }
        }
        return filteredList;
    }
    
    public ShopItem GetItem(Item item)
    {
        foreach (var itemSet in ItemSets)
        {
            foreach (var element in itemSet.Set)
            {
                if ((element.Item as Item).ID == (item.ID))
                {
                    return element;
                }
            }
        }
        return null;
    }
    
    public void RemoveItem(ShopItem item, int quantity)
    {
        foreach (var itemSet in ItemSets)
        {
            foreach (var element in itemSet.Set)
            {
                if ((element.Item as Item).ID == (item.Item as Item).ID)
                {                       
                    element.Quantity -= quantity;
                }
            }
        }
    }
}

[Serializable]
public class ShopItemSet
{
    public int ID;
    public bool Unlocked;
    public ShopItem[] Set;

    public ShopItemSet(int iD, bool unlocked, ShopItem[] set)
    {
        ID = iD;
        Unlocked = unlocked;
        Set = set;
    }
}

[Serializable]
public class ShopItem
{
    public Item Item;
    public int Quantity;
}

[Serializable]
public class SerializedShop : SerializableData
{
    public Dictionary<int, SerializedShopItemSet> ItemSets;
}

[Serializable]
public class SerializedShopItemSet
{
    public bool Unlocked;
    public Dictionary<int, int> Items;
}
