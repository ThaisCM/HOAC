﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Zone", menuName = "G1/Diary/Quest System/Zone")]
public class Zone : ScriptableObject, IDatabaseAsset
{
   public int ID;
   public string NameKey;
   public GameObject ZoneTitlePrefab;

   public int Id
   {
      get { return ID;}
      set { ID = value; }
   }
   
   public string Name { get { return NameKey; } set { NameKey = value; } }
}
