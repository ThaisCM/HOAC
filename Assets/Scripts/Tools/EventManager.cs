﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct GameEvent
{
    public const string PAUSE = "Pause";
    public const string UNPAUSE = "Unpause";
    public const string LOAD = "Load";

    public string EventName;
    public GameEvent(string newName)
    {
        EventName = newName;
    }
}

public static class EventManager 
{
    private static Dictionary<Type, List<EventListenerBase>> m_SubscribersList;

    static EventManager()
    {
        m_SubscribersList = new Dictionary<Type, List<EventListenerBase>>();
    }

    public static void AddListener<Event>(EventListener<Event> listener) where Event : struct
    {
        Type eventType = typeof(Event);

        if (!m_SubscribersList.ContainsKey(eventType))
        {
            m_SubscribersList[eventType] = new List<EventListenerBase>();
        }

        if(!SubscriptionExists(eventType, listener))
        {
            m_SubscribersList[eventType].Add(listener);
        }
    }

    public static void RemoveListener<Event>(EventListener<Event> listener) where Event : struct
    {
        Type eventType = typeof(Event);

        if (!m_SubscribersList.ContainsKey(eventType))
        {
            return;
        }

        List<EventListenerBase> subscriberList = m_SubscribersList[eventType];
        bool listenerFound;
        listenerFound = false;

        if (listenerFound)
        {

        }

        for (int i = 0; i < subscriberList.Count; i++)
        {
            if (subscriberList[i] == listener)
            {
                subscriberList.Remove(subscriberList[i]);
                listenerFound = true;

                if (subscriberList.Count == 0)
                    m_SubscribersList.Remove(eventType);

                return;
            }
        }
    }

    public static void TriggerEvent<Event>(Event newEvent) where Event : struct
    {
        List<EventListenerBase> list;
        if (!m_SubscribersList.TryGetValue(typeof(Event), out list))
            return;

        for (int i = 0; i < list.Count; i++)
        {
            (list[i] as EventListener<Event>).OnEvent(newEvent);
        }
    }

    private static bool SubscriptionExists(Type type, EventListenerBase receiver)
    {
        List<EventListenerBase> receivers;

        if (!m_SubscribersList.TryGetValue(type, out receivers)) return false;

        bool exists = false;

        for (int i = 0; i < receivers.Count; i++)
        {
            if (receivers[i] == receiver)
            {
                exists = true;
                break;
            }
        }

        return exists;
    }
}

public static class EventRegister
{
    public delegate void Delegate<T>(T eventType);

    public static void EventStartListening<EventType>(this EventListener<EventType> caller) where EventType : struct
    {
        EventManager.AddListener<EventType>(caller);
    }

    public static void EventStopListening<EventType>(this EventListener<EventType> caller) where EventType : struct
    {
        EventManager.RemoveListener<EventType>(caller);
    }
}

public interface EventListenerBase { };

public interface EventListener<T> : EventListenerBase
{
    void OnEvent(T eventType);
}

