﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIEffects : MonoBehaviour
{
    public RectTransform Layout;
    public GameObject EffectPrefab;

    public void DisplayEffects(List<Effect> effects)
    {
        EraseContent();
        for (int i = 0; i < effects.Count; i++)
        {
            var effect = effects[i];
            if (effect.IsAplied)
            {
                GUIEffectSlot slot = Instantiate(EffectPrefab, Layout).GetComponent<GUIEffectSlot>();
                slot.Init(effect);
            }
        }
    }
    
    public void EraseContent()
    {
        foreach (RectTransform child in Layout)
        {
            Destroy(child.gameObject);
        }
        Layout.DetachChildren();
    }
}
