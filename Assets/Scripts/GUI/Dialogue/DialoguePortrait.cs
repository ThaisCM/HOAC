﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialoguePortrait : MonoBehaviour
{
    public enum Mode {Black, White}

    public Image Image;

    public void Init(Sprite portrait)
    {
        Image.sprite = portrait;
    }
    
    public void Show(Mode mode)
    {
        switch (mode)
        {
            case Mode.Black:
                Image.color = new Color(0.25f, 0.25f, 0.25f);
                break;
            case Mode.White:
                Image.color = new Color(1f,1f,1f);
                break;
        }
    }

    public void Hide()
    {
        Image.color = new Color(0.25f, 0.25f, 0.25f);
    }
}
